package com.example.shahar.testproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class Login extends AppCompatActivity {

    private FirebaseUser user;
    private Button logoutBtn;
    private ImageView profileImg;
    private FirebaseAuth mAuth;
    private AccessToken token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        Intent intent = getIntent();

        user = intent.getParcelableExtra("user");
        token = intent.getParcelableExtra("token");
        profileImg = findViewById(R.id.profile_pic);

        GraphRequest request = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        try {
                            String name = object.getString("first_name");
                            ((TextView)findViewById(R.id.welcome_txt)).setText("Hello "+name+"!");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,link,name");
        request.setParameters(parameters);
        request.executeAsync();

        //set profile picture
        String image_url = "http://graph.facebook.com/" + token.getUserId() + "/picture?type=large";
        Glide.with(Login.this)
                .load(image_url)
                .into(profileImg);

        //set logout btn
        logoutBtn = findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuth.signOut();
                LoginManager.getInstance().logOut();

                updateUI();
            }
        });

    }

    public void onStart(){
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null)
            updateUI();
    }

    private void updateUI() {
        Toast.makeText(Login.this, "You're logged out",Toast.LENGTH_LONG).show();
        Intent loginWelcomeIntent = new Intent(Login.this, MainActivity.class);
        startActivity(loginWelcomeIntent);
        finish();
    }
}
